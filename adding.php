<?php
$error = '';
$success = '';
if (isset($_POST['submit'])) {
    if (empty($_POST['name'])){
        $error .= "<br />Please enter film.";
    }
    if (empty($_POST['year'])){
        $error .= "<br />Please enter year.";
    } else {
        if (!is_numeric($_POST['year'])) {
            $error .= "<br />Please enter correct year.";
        }
        if (strlen($_POST['year']) != 4) $error .= "<br />Year must have four digits.";
    }
    if ($error){
        $error = "There is some problem".$error;
    } else{
        $link = mysqli_connect('localhost','root', '', 'films');

        $film_for_query = mysqli_real_escape_string($link, $_POST['name']);

        $year_for_query = $_POST['year'] ;

        $query = "SELECT * FROM films WHERE name='$film_for_query'";

        $result = mysqli_query($link, $query);

        $results = mysqli_num_rows($result);

        if ($results){
            $error .= "This film is already added.";
        }

        else{
            $query = "INSERT INTO `films` (`name`, `year`) VALUES ('$film_for_query', '$year_for_query' )";

            mysqli_query($link, $query);

            $success= "You&#39ve added new film.";
        }

    }
}
?>