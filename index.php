<?php

   include("adding.php");

?>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Bootstrap 101 Template</title>

    <!-- Bootstrap -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <style type="text/css">

        #topContainer{
            background-image: url("images/rsz_fon.jpg");
            width:100%;
            background-size: cover;
        }
        #topRow{
            margin-top:100px;
        }
        .center{
            text-align: center;

        }

        .marginTop{
            margin-top: 30px;
        }

        .nextButton{
            margin-top: 50px;
        }

    </style>



</head>
<body>

<div class="container contentContainer" id="topContainer">
    <div class="row">


        <div class="col-md-6 col-md-offset-3" id="topRow">
            <h1 class="marginTop center">List of favorite films</h1>


            <h3 class="center marginTop">Add to the list a film that you like.</h3>


                <?php

                    if($error){
                        echo '<div class="center col-lg-12 alert alert-warning">'.addslashes($error).'</div>';
                    }

                    if($success){
                        echo '<div class="center col-lg-12 alert alert-success">'.addslashes($success).'</div>';
                    }

                ?>


                <form role="form" method="post">
                    <div class="form-group">
                        <div class="col-xs-4">
                            <input class="form-control" type="text" name="name" id="name" placeholder="Name of a film">
                        </div>
                        <div class="col-xs-4">
                            <input class="form-control" type="number" name="year" id="year" placeholder="Year, e.g. 1880">
                        </div>
                        <div class="col-xs-4">
                            <input class="btn btn-primary btn-block btn-success" type="submit" name="submit" value="Add film">
                        </div>

                        <div class="col-xs-12 nextButton">
                            <a class="btn btn-lg btn-primary btn-block" href="list.php" >Film list</a>


                        </div>
                    </div>

                </form>




        </div>
    </div>
</div>


<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
<!-- Include all compiled plugins (below), or include individual files as needed -->
<script src="js/bootstrap.min.js"></script>
<script>

    $(".contentContainer").css("min-height",$(window).height());

</script>
</body>
</html>





